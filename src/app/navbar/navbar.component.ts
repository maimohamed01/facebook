import { Component, OnInit,ElementRef } from '@angular/core';
import *  as $ from "jquery";
import { userData } from '../model/userData';
import {FaceservicesService} from '../services/faceservices.service'
declare var $:any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  userData:userData=new userData();
  submitted:boolean=false;
  constructor(private elementRef:ElementRef,private FaceservicesService:FaceservicesService) { }
  ngOnInit() {
    $(".fa-bell").click(function(){
      $(".hidden-list").fadeToggle(2);
  }); 
  $(".fa-user").click(function(){
      $(".hidden-list-friends-request").fadeToggle(2);
  }); 
  $(".fa-caret-down").click(function(){
      $(".hidden-list-caret").fadeToggle(2);

  });   
  $(".fa-question-circle").click(function(){
      $(".hidden-list-message").fadeToggle(2); 
   }); 
   $(".fa-comment").click(function(){
      $(".hidden-list-question").fadeToggle(2); 
   });
  }
  saveData(){
    const data={
      postcontent:this.userData.postcontent,
      imgurl:this.userData.imgurl
    };
    console.log(data);
    this.FaceservicesService.createuserData(data).subscribe(
     response => {
      this.submitted = true;
        console.log(response);
     }
     );
  }
   newTut(){
    this.submitted = false;
     this.userData={
      postcontent:'',
      imgurl:'',
      
     
     };
   }
}

