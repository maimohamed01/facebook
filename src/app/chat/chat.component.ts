import { Component, OnInit,ElementRef } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  constructor(private _elementRef:ElementRef) { }

  ngOnInit() {
    $('.dashboard li').click(function(){
      $(this).addClass('selected').siblings('li').removeClass('selected');
      $('.chat').hide();
      $('.'+$(this).data('class')).fadeIn();
      });
      $('.fa-times').click(function(){
        $('.chat').hide();
    });
  }
}