import { Component, OnInit,ElementRef } from '@angular/core';
import { userData } from '../model/userData';
import {FaceservicesService} from '../services/faceservices.service'
declare var $: any;
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
   userData:userData=new userData();
   public postcont=this.userData.postcontent;
  constructor(private _elementRef:ElementRef,private FaceservicesService:FaceservicesService) {
    
   }
  submitted:boolean=false;
  ngOnInit() {
   $(".btn-post").click(function(){
      $(".postsCreated").text( $("textarea").val());
       $(".postsCreated").css({background:"white", "margin-top":"2%" ,padding:"20px"})
   });
  }
 

 btnpost(){
   document.getElementById('lighter').classList.add("show");
   document.getElementById('headerPost').classList.remove("show");
    document.getElementById('line3').classList.remove("show");
    document.getElementById('line2').classList.remove("show");
    document.getElementById('postsCreatedpart2').classList.remove("show");
 }
 wclose(){
   document.getElementById('lighter').classList.add("show");
 }
}
